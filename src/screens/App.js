import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';
import { setText, getGoogle, increment, decrement } from '../actions';

const data = [
  {
    id: '1',
    name: 'Super​ ​iPad',
    key: 'ipd',
    price: 549.99,
    amountTaken: 3
  }, {
    id: '2',
    name: 'MacBook​ ​Pro',
    key: 'mbp',
    price: 1399.99,
    amountTaken: 4
  }, {
    id: '3',
    name: 'Apple​ ​TV',
    key: 'atv',
    price: 109.50,
    amountTaken: 2
  }, {
    id: '4',
    name: 'VGA​ ​adapter',
    key: 'vga',
    price: 30.00,
    amountTaken: 3
  },
];

const styles = StyleSheet.create({
  container: {
    //justifyContent: 'center',
    //alignItems: 'center',
    flex: 4,
    backgroundColor: '#DCDCDC'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  containerStyle: {
    flexDirection: 'row',
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#e2e2e2',
    padding: 10,
    paddingLeft: 15,
    backgroundColor: '#fff'
  },
  lastItemStyle: {
    flexDirection: 'row',
    flex: 1,
    padding: 10,
    paddingLeft: 15,
    backgroundColor: '#fff'
  },
  imageStyle: {
    width: 50,
    height: 50,
    marginRight: 20
  },
  textStyle: {
    flex: 2,
    justifyContent: 'center'
  },
  priceStyle: {
    backgroundColor: '#ddd',
    width: 60,
    alignItems: 'center',
    marginTop: 3,
    borderRadius: 3
  },
  counterStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  totalStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export class App extends Component {

  // updateAddCart = (item) => {
  //   console.log(item.key, ' ', this.state.itemCount[item.key]);
  //   this.setState({
  //     ...this.state,
  //     itemCount: {
  //       ...this.state.itemCount,
  //       [item.key]: this.state.itemCount[item.key] + 1,
  //     },
  //   }, this.calculateTotal);
  // };

  // updateDeleteCart = (item) => {
  //   this.setState({
  //     ...this.state,
  //     itemCount: {
  //       ...this.state.itemCount,
  //       [item.key]: this.state.itemCount[item.key] > 0 ? this.state.itemCount[item.key] - 1 : 0,
  //     },
  //   }, this.calculateTotal);
  // };

  // calculateTotal = () => {
  //   const {
  //     itemCount: {
  //       ipd,
  //       mbp,
  //       atv,
  //       vga,
  //     }
  //   } = this.state;
  //   const totalVal = (ipd * 549.99) + (mbp * 1399.99) + (atv * 109.50) + (vga * 30.00);
  //   this.setState({
  //     ...this.state,
  //     total: totalVal,
  //   });
  // }

  _renderItem = ({ item, index }) => {
    const {
      containerStyle,
      lastItemStyle,
      textStyle,
      counterStyle,
      priceStyle
    } = styles;

    return (
      <View key={index} style={(index + 1 === data.length) ? lastItemStyle : containerStyle}>
        <View style={textStyle}>
          <Text style={{ color: '#2e2f30' }}>{item.name}</Text>
          <View style={priceStyle}>
            <Text style={{ color: '#2e2f30', fontSize: 12 }}>${item.price}</Text>
          </View>
        </View>

        <View style={counterStyle}>
          <Icon.Button
            name="ios-remove"
            size={25}
            color="#fff"
            backgroundColor="#fff"
            style={{
              borderRadius: 15, backgroundColor: '#bbb', height: 30, width: 30
            }}
            iconStyle={{ marginRight: 0 }}
            onPress={() => this.props.decrement(item)}
          />

          <Text>{this.props.itemCount[item.key]}</Text>

          <Icon.Button
            name="ios-add"
            size={25}
            color="#fff"
            backgroundColor="#fff"
            style={{
              borderRadius: 15, backgroundColor: '#bbb', height: 30, width: 30
            }}
            iconStyle={{ marginRight: 0 }}
            onPress={() => this.props.increment(item)}
          />

        </View>
      </View>);
  }

  render() {
    return (
      <View style={styles.container}>

        <FlatList
          data={data}
          renderItem={this._renderItem}
          keyExtractor={item => item.id}
          extraData={this.props}
        />
        <View style={styles.totalStyle}>
          <Text>Total - </Text>
          <Text>${parseFloat(this.props.total).toFixed(2)}</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  text: state.testReducer.text,
  status: state.testReducer.status,
  itemCount: state.testReducer.itemCount,
  total: state.testReducer.total,
});

export default connect(mapStateToProps, {
  setText, getGoogle, increment, decrement
})(App);
