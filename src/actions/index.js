import { CHANGE_TEXT, GET_GOOGLE, INCREMENT, DECREMENT } from '../constants';

export function setText(text) { //eslint-disable-line
  return {
    type: CHANGE_TEXT,
    text
  };
}

export function increment(item) { //eslint-disable-line
  return {
    type: INCREMENT,
    item
  };
}

export function decrement(item) { //eslint-disable-line
  return {
    type: DECREMENT,
    item
  };
}

export function getGoogle() { //eslint-disable-line
  return {
    type: GET_GOOGLE
  };
}
