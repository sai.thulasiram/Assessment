import { CHANGE_TEXT, INCREMENT, DECREMENT } from '../constants';

const initialState = {
  text: 'from Redux',
  itemCount: {
    ipd: 0,
    mbp: 0,
    atv: 0,
    vga: 0,
  },
  total: 0,
};

function getAppleTVPrice(atv) {
  if (atv > 2) {
    const offerPrice = atv / 3;
    const actPrice = atv % 3;

    return (offerPrice * 2 * 109.50) + (actPrice * 109.50);
  }
  return atv * 109.50;
}

function getIPDPrice(ipd) {
  if (ipd > 4) {
    return (ipd * 499.99);
  }
  return ipd * 549.99;
}

function getvgaPrice(vga, mbp) {
  const count = vga - mbp;
  return count * 30.00;
}

function calculateTotal(itemCount) {
  const {
    ipd,
    mbp,
    atv,
    vga,
  } = itemCount;
  const totalVal = getIPDPrice(ipd) + (mbp * 1399.99) + getAppleTVPrice(atv) + getvgaPrice(vga, mbp);
  return totalVal;
}

export default function testeReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_TEXT:
      return {
        ...state,
        text: action.text
      };
    case 'GET_GOOGLE_SUCCESS':
      return {
        ...state,
        status: action.status
      };
    case INCREMENT: // eslint-disable-line
      const itemCount = {
        ...state.itemCount,
        [action.item.key]: state.itemCount[action.item.key] + 1,
      };

      return {
        ...state,
        itemCount: {
          ...itemCount,
        },
        total: calculateTotal(itemCount),
      };
    case DECREMENT: // eslint-disable-line
      const deleteitemCount = {
        ...state.itemCount,
        [action.item.key]: state.itemCount[action.item.key] > 0 ? state.itemCount[action.item.key] - 1 : 0,
      };
      return {
        ...state,
        itemCount: {
          ...deleteitemCount,
        },
        total: calculateTotal(deleteitemCount),
      };
    default:
      return state;
  }
}
